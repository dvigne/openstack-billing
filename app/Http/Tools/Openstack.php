<?php

/**
* Openstack API Driver Library
* Author: Derick Vigne
* Last Modified: 6-11-2018
*/

// TODO: Fix API getAuthToken() method to first create a project
// for the new users then scope to that project before returning X-Auth-Token
namespace App\Http\Tools;

use Guzzle;
use Config;
use GuzzleHttp\Exception\ClientException;
use Log;

class Openstack
{
  private $username;
  private $password;
  private $domain;
  private $url;
  private $client;
  private $token = null;

  function __construct()
  {
    $this->username = Config::get('services.openstack.username');
    $this->password = Config::get('services.openstack.password');
    $this->domain = Config::get('services.openstack.domain');
    $this->url = Config::get('services.openstack.url');
    $this->client = new Guzzle([
      'base_uri' => $this->url
    ]);
  }

  public function createProject()
  {
    // TODO: Use unscopped authentcation to create users project
  }
  public function getAuthToken()
  {
    if ($this->token == null) {
      $response = $this->client->request('POST', 'v3/auth/tokens', [
        'json' => [
          'auth' => [
            'identity' => [
              'methods' => [
                'password'
              ],
              'password' => [
                'user' => [
                  'name' => $this->username,
                  'domain' => [
                    'name' => $this->domain
                  ],
                  'password' => $this->password
                ]
              ]
            ],
            "scope" => [
              "project" => [
                "id" => "d0645f9574f64c16af8f22ba7434611a" // Temporary workaround for scoped authentication
              ]
            ]
          ]
        ]
      ]);
      $this->token = $response->getHeader('X-Subject-Token')[0];
      return $this->token;
    }
    else {
      return $this->token;
    }
  }
  public function createUser($name, $email, $password, $token)
  {
    try {
      $response = $this->client->request('POST', 'v3/users', [
        'headers' => [
          'X-Auth-Token' => $token,
        ],
        'json' => [
          'user' => [
            'default_project_id' => 'd0645f9574f64c16af8f22ba7434611a',
            'enabled' => true,
            'name' => $name,
            'password' => $password,
            'description' => 'Openstack-billing User',
            'email' => $email
          ]
        ]
      ]);
    }
    catch (ClientException $e) {
      Log::debug($e);
      return false;
    }
    return true;
  }

  public function disableUser($userID)
  {
    # TODO: Write Guzzle client request to update a user to disbled if they miss payment
  }

  public function shutdownVirtualMachines($userID)
  {
    # TODO: Write Guzzle client request to power off all user vm's until bill is payed
  }

  public function enableUser($userID)
  {
    # TODO: Write Guzzle client request to enable a user after payment is processed
  }
}
